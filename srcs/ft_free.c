/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_free.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/09/27 17:02:09 by garm              #+#    #+#             */
/*   Updated: 2014/09/28 05:01:05 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

void		ft_unlink_first_chunk(int type)
{
	if (type == TINY)
		g_env.tiny = g_env.tiny->next;
	else if (type == SMALL)
		g_env.small = g_env.small->next;
	else
		g_env.large = g_env.large->next;
}

void		ft_free_chunk(t_chunk *chunk)
{
	t_data		*datablock;
	size_t		len;

	if (chunk)
	{
		if (chunk->type == TINY)
			len = TINY_SIZE;
		else if (chunk->type == SMALL)
			len = SMALL_SIZE;
		else
		{
			datablock = (t_data *)((char *)chunk + sizeof(t_chunk));
			len = datablock->size + sizeof(t_data) + sizeof(t_chunk);
		}
		munmap((void *)chunk, len);
	}
}

void		ft_destroy_chunk(t_chunk *chunk)
{
	t_chunk		*list;
	t_chunk		*temp;

	if (chunk->type == TINY)
		list = g_env.tiny;
	else if (chunk->type == SMALL)
		list = g_env.small;
	else
		list = g_env.large;
	temp = NULL;
	while (list && list != chunk)
	{
		temp = list;
		list = list->next;
	}
	if (list && !temp)
		ft_unlink_first_chunk(chunk->type);
	else if (list && temp)
		temp->next = list->next;
	ft_free_chunk(chunk);
}

void		free(void *ptr)
{
	t_chunk		*chunk;
	t_data		*datablock;

	if (ptr)
	{
		datablock = (t_data *)((char *)ptr - sizeof(t_data));
		datablock->free = 1;
		if (datablock->next && datablock->next->free)
			datablock = ft_defragment(datablock);
		if (datablock->prev && datablock->prev->free)
			datablock = ft_defragment(datablock->prev);
		chunk = ft_rewind_chunk(datablock);
		if (chunk->free_data && chunk->free_data->size < datablock->size)
			chunk->free_data = datablock;
		if (!datablock->prev && !datablock->next && datablock->free)
			ft_destroy_chunk(chunk);
	}
}
