/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/09/28 04:41:01 by garm              #+#    #+#             */
/*   Updated: 2014/09/28 05:05:34 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

void		ft_set_high_block(t_chunk *chunk)
{
	t_data		*cursor;
	t_data		*highest_block;

	cursor = (t_data *)((char *)chunk + sizeof(t_chunk));
	highest_block = cursor;
	while (cursor)
	{
		if (cursor->size > highest_block->size && cursor->free)
			highest_block = cursor;
		cursor = cursor->next;
	}
	chunk->free_data = highest_block;
}

void		ft_link_datablock(t_data *datablock, t_data *free_block)
{
	if (datablock && free_block)
	{
		free_block->prev = datablock;
		free_block->next = NULL;
		if (datablock->next)
		{
			datablock->next->prev = free_block;
			free_block->next = datablock->next;
		}
		datablock->next = free_block;
	}
}

void		*ft_split_datablock(t_chunk *chunk, size_t size)
{
	t_data		*datablock;
	t_data		*free_block;
	size_t		total_size;
	size_t		new_free_size;

	total_size = (chunk->free_data) ? (chunk->free_data->size) : 0;
	new_free_size = total_size - sizeof(t_data) - size;
	datablock = chunk->free_data;
	datablock->free = 0;
	if ((int)(total_size - sizeof(t_data) - sizeof(t_data) - size) < 0)
	{
		chunk->free_data = NULL;
		return (datablock->data);
	}
	datablock->size = size;
	free_block = (t_data *)((char *)datablock + size + sizeof(t_data));
	ft_link_datablock(datablock, free_block);
	chunk->free_data = free_block;
	free_block->free = 1;
	free_block->size = new_free_size;
	free_block->data = (void *)((char *)free_block + sizeof(t_data));
	ft_set_high_block(chunk);
	return (datablock->data);
}
