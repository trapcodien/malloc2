/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_defragment.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/09/28 04:38:19 by garm              #+#    #+#             */
/*   Updated: 2014/09/28 05:04:17 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

t_data		*ft_defragment(t_data *datablock)
{
	if (datablock && datablock->next)
	{
		datablock->size += datablock->next->size + sizeof(t_data);
		if (datablock->next->next)
			datablock->next->next->prev = datablock;
		datablock->next = datablock->next->next;
	}
	return (datablock);
}

t_chunk		*ft_rewind_chunk(t_data *datablock)
{
	t_chunk		*chunk;

	chunk = NULL;
	if (datablock)
	{
		while (datablock->prev)
			datablock = datablock->prev;
		chunk = (t_chunk *)((char *)datablock - sizeof(t_chunk));
	}
	return (chunk);
}
