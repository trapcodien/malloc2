/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_malloc.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/09/24 19:15:53 by garm              #+#    #+#             */
/*   Updated: 2014/09/28 05:00:17 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

void			*malloc(size_t size)
{
	t_chunk			*chunk;
	int				chunk_type;
	static int		first = 1;

	if (first)
	{
		first = 0;
		ft_memset(&g_env, 0, sizeof(t_env));
	}
	chunk_type = ft_get_chunk_type(size);
	chunk = ft_find_chunk(size, chunk_type);
	if (chunk)
		return (ft_split_datablock(chunk, size));
	if (!chunk && chunk_type < LARGE)
		chunk = ft_create_chunk(size, chunk_type);
	else
		chunk = ft_create_large_chunk(size);
	ft_add_chunk(chunk, size);
	return (GET_FIRST_DATABLOCK(chunk));
}

size_t			ft_get_chunk_size(size_t size)
{
	if (size >= 1 && size <= TINY_LIMIT)
		return (TINY_SIZE);
	else if (size > TINY_LIMIT && size <= SMALL_LIMIT)
		return (SMALL_SIZE);
	else
		return (size + sizeof(t_chunk) + sizeof(t_data));
}

int				ft_get_chunk_type(size_t size)
{
	if (size >= 1 && size <= TINY_LIMIT)
		return (TINY);
	else if (size > TINY_LIMIT && size <= SMALL_LIMIT)
		return (SMALL);
	else
		return (LARGE);
}
