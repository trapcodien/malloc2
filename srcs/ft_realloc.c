/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_realloc.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/09/28 02:56:18 by garm              #+#    #+#             */
/*   Updated: 2014/09/28 05:05:34 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

void		*realloc(void *ptr, size_t size)
{
	void		*new;
	t_data		*datablock;
	size_t		len;

	new = NULL;
	if (ptr)
	{
		datablock = (t_data *)((char *)ptr - sizeof(t_data));
		len = (size < datablock->size) ? (size) : (datablock->size);
		new = malloc(size);
		ft_memcpy(new, ptr, len);
		free(ptr);
	}
	return (new);
}
