/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_chunk.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/09/28 04:39:34 by garm              #+#    #+#             */
/*   Updated: 2014/09/28 05:03:47 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

t_chunk		*ft_find_chunk(size_t size, int chunk_type)
{
	t_chunk		*chunk;

	if (chunk_type == TINY)
		chunk = g_env.tiny;
	if (chunk_type == SMALL)
		chunk = g_env.small;
	if (chunk_type == LARGE)
		chunk = g_env.large;
	while (chunk)
	{
		if (chunk->free_data && size + sizeof(t_data) <= chunk->free_data->size)
			return (chunk);
		chunk = chunk->next;
	}
	return (NULL);
}

void		*ft_alloc_chunk(size_t size)
{
	int		prot;
	int		flags;
	void	*memory;

	prot = PROT_READ | PROT_WRITE;
	flags = MAP_ANON | MAP_PRIVATE;
	memory = mmap(NULL, size, prot, flags, -1, 0);
	if (memory == MAP_FAILED)
		return (NULL);
	else
		return (memory);
}

t_chunk		*ft_create_chunk(size_t size, int chunk_type)
{
	size_t		chunk_size;
	t_data		*data;
	t_data		*free_data;
	t_chunk		*new_chunk_head;
	void		*mem;

	chunk_size = ft_get_chunk_size(size);
	if (!(mem = ft_alloc_chunk(chunk_size)))
		return (NULL);
	new_chunk_head = (t_chunk *)mem;
	new_chunk_head->type = chunk_type;
	data = (t_data *)((char *)mem + sizeof(t_chunk));
	data->size = size;
	data->next = (t_data *)((char *)data + sizeof(t_data) + size);
	data->prev = NULL;
	data->data = (void *)((char *)data + sizeof(t_data));
	data->free = 0;
	free_data = (t_data *)((char *)data + sizeof(t_data) + size);
	new_chunk_head->free_data = free_data;
	free_data->size = chunk_size - sizeof(t_chunk) - sizeof(t_data) * 2 - size;
	free_data->next = NULL;
	free_data->prev = data;
	free_data->free = 1;
	free_data->data = (void *)((char *)free_data + sizeof(t_data));
	return (new_chunk_head);
}

t_chunk		*ft_create_large_chunk(size_t size)
{
	t_data		*data;
	t_chunk		*new_chunk_head;
	void		*mem;

	mem = ft_alloc_chunk(size + sizeof(t_chunk) + sizeof(t_data));
	if (!mem)
		return (NULL);
	new_chunk_head = (t_chunk *)mem;
	new_chunk_head->type = LARGE;
	new_chunk_head->free_data = NULL;
	data = (t_data *)((char *)mem + sizeof(t_chunk));
	data->size = size;
	data->next = NULL;
	data->prev = NULL;
	data->data = (void *)(data + 1);
	data->free = 0;
	return (new_chunk_head);
}

void		ft_add_chunk(t_chunk *chunk, size_t size)
{
	if (chunk)
	{
		if (size >= 1 && size <= TINY_LIMIT)
		{
			chunk->next = g_env.tiny;
			g_env.tiny = chunk;
		}
		else if (size > TINY_LIMIT && size <= SMALL_LIMIT)
		{
			chunk->next = g_env.small;
			g_env.small = chunk;
		}
		else
		{
			chunk->next = g_env.large;
			g_env.large = chunk;
		}
	}
}
