/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   show_alloc_mem.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/09/27 23:30:12 by garm              #+#    #+#             */
/*   Updated: 2014/09/28 04:58:52 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

void		ft_put_addr(void *addr)
{
	ft_putstr("0x");
	ft_putnbr_base((unsigned long)addr, "0123456789ABCDEF");
}

t_chunk		*ft_catch_next_addr(t_chunk *addr, t_chunk *lowest)
{
	t_chunk			*cursor;

	cursor = g_env.tiny;
	while (cursor)
	{
		if ((!lowest || cursor < lowest) && cursor > addr)
			lowest = cursor;
		cursor = cursor->next;
	}
	cursor = g_env.small;
	while (cursor)
	{
		if ((!lowest || cursor < lowest) && cursor > addr)
			lowest = cursor;
		cursor = cursor->next;
	}
	cursor = g_env.large;
	while (cursor)
	{
		if ((!lowest || cursor < lowest) && cursor > addr)
			lowest = cursor;
		cursor = cursor->next;
	}
	return ((addr == lowest) ? (NULL) : (lowest));
}

void		ft_put_type(int type)
{
	if (type == TINY)
		ft_putstr("TINY : ");
	else if (type == SMALL)
		ft_putstr("SMALL : ");
	else
		ft_putstr("LARGE : ");
}

void		ft_print_range(t_data *datablock)
{
	void		*begin;
	void		*end;

	begin = (void *)((char *)datablock + sizeof(t_data));
	end = (void *)((char *)datablock + datablock->size + sizeof(t_data));
	ft_put_addr(begin);
	ft_putstr(" - ");
	ft_put_addr(end);
	ft_putstr(" : ");
	ft_putnbr_base((long)datablock->size, "0123456789");
	ft_putstr(" octets");
	ft_putendl(NULL);
}

void		show_alloc_mem(void)
{
	t_chunk		*cursor;
	t_data		*datablock;

	cursor = NULL;
	cursor = ft_catch_next_addr(cursor, NULL);
	while (cursor)
	{
		ft_put_type(cursor->type);
		ft_put_addr((void *)cursor);
		ft_putendl(NULL);
		datablock = (t_data *)((char *)cursor + sizeof(t_chunk));
		while (datablock)
		{
			if (!datablock->free)
				ft_print_range(datablock);
			datablock = datablock->next;
		}
		cursor = ft_catch_next_addr(cursor, NULL);
	}
}
