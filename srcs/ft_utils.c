/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_utils.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/09/28 03:20:16 by garm              #+#    #+#             */
/*   Updated: 2014/09/28 05:02:30 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

void		ft_putchar(char c)
{
	write(1, &c, 1);
}

void		*ft_memset(void *b, int c, size_t len)
{
	size_t		i;
	char		*memory;

	if (!b)
		return (NULL);
	memory = (char *)b;
	i = 0;
	while (i < len)
	{
		memory[i] = c;
		i++;
	}
	return (b);
}

void		*ft_memcpy(void *s1, const void *s2, size_t n)
{
	size_t			i;
	char			*str1;
	const char		*str2;

	if (!s1 || !s2)
		return (s1);
	str1 = (char *)s1;
	str2 = (const char *)s2;
	i = 0;
	while (i < n)
	{
		str1[i] = str2[i];
		i++;
	}
	return (s1);
}

void		ft_putstr(char *str)
{
	write(1, str, ft_strlen(str));
}

void		ft_putendl(char *str)
{
	ft_putstr(str);
	ft_putchar('\n');
}
