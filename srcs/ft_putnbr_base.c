/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_base.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/07/10 17:10:35 by garm              #+#    #+#             */
/*   Updated: 2014/09/28 04:59:29 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

static void		ft_putnbr_b(long nbr, char *base, long base_len)
{
	if (nbr < 0)
	{
		nbr = nbr * -1;
		ft_putchar('-');
	}
	if (nbr >= base_len)
	{
		ft_putnbr_b(nbr / base_len, base, base_len);
		ft_putnbr_b(nbr % base_len, base, base_len);
	}
	else
	{
		nbr = base[nbr];
		ft_putchar((char)nbr);
	}
}

static long		ft_check(char *b)
{
	long		i;
	long		j;

	i = 0;
	while (b[i])
	{
		j = 0;
		while (b[j])
		{
			if (b[j] == '+' || b[j] == '-' || (i != j && b[i] == b[j]))
				return (0);
			j++;
		}
		i++;
	}
	return (1);
}

long			ft_strlen(const char *str)
{
	long		i;

	if (!str)
		return (0);
	i = 0;
	while (str[i])
		i++;
	return (i);
}

void			ft_putnbr_base(long nbr, char *base)
{
	long		base_len;

	base_len = ft_strlen(base);
	if (base_len >= 2 && ft_check(base))
		ft_putnbr_b(nbr, base, base_len);
}
