/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/09/23 12:44:57 by garm              #+#    #+#             */
/*   Updated: 2014/09/28 05:05:31 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MALLOC_H
# define MALLOC_H

# include <stddef.h>
# include <sys/mman.h>

# define TINY 1
# define TINY_LIMIT 512
# define TINY_SIZE 4096

# define SMALL 2
# define SMALL_LIMIT 4096
# define SMALL_SIZE 65536

# define LARGE 3

# define GET_FIRST_DATABLOCK(c) ((char *)c + sizeof(t_chunk) + sizeof(t_data))

typedef struct		s_chunk
{
	int				type;
	struct s_chunk	*next;
	struct s_data	*free_data;
}					t_chunk;

typedef struct		s_data
{
	size_t			size;
	struct s_data	*next;
	struct s_data	*prev;
	void			*data;
	int				free;
}					t_data;

typedef struct		s_env
{
	t_chunk			*tiny;
	t_chunk			*small;
	t_chunk			*large;
}					t_env;

t_env				g_env;

/*
** show_alloc_mem.c
*/
void				ft_put_addr(void *addr);
t_chunk				*ft_catch_next_addr(t_chunk *addr, t_chunk *lowest);
void				ft_put_type(int type);
void				ft_print_range(t_data *datablock);
void				show_alloc_mem(void);

/*
** ft_putnbr_base.c
*/
void				ft_putnbr_base(long nbr, char *base);
long				ft_strlen(const char *str);

/*
** ft_utils.c
*/
void				ft_putchar(char c);
void				*ft_memset(void *b, int c, size_t len);
void				*ft_memcpy(void *s1, const void *s2, size_t n);
void				ft_putstr(char *str);
void				ft_putendl(char *str);

/*
** ft_chunk.c
*/
t_chunk				*ft_find_chunk(size_t size, int chunk_type);
void				*ft_alloc_chunk(size_t size);
t_chunk				*ft_create_chunk(size_t size, int chunk_type);
t_chunk				*ft_create_large_chunk(size_t size);
void				ft_add_chunk(t_chunk *chunk, size_t size);

/*
** ft_defragment.c
*/
t_data				*ft_defragment(t_data *datablock);
t_chunk				*ft_rewind_chunk(t_data *datablock);

/*
** ft_split.c
*/
void				ft_set_high_block(t_chunk *chunk);
void				ft_link_datablock(t_data *datablock, t_data *free_block);
void				*ft_split_datablock(t_chunk *chunk, size_t size);

/*
** ft_malloc.c
*/
void				*malloc(size_t size);
size_t				ft_get_chunk_size(size_t size);
int					ft_get_chunk_type(size_t size);

/*
** ft_free.c
*/
void				free(void *ptr);
void				ft_destroy_chunk(t_chunk *chunk);
void				ft_free_chunk(t_chunk *chunk);
void				ft_unlink_first_chunk(int type);

/*
** ft_realloc.c
*/
void				*realloc(void *ptr, size_t size);

#endif
