# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: garm <garm@student.42.fr>                  +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2013/08/26 23:49:31 by garm              #+#    #+#              #
##   Updated: 2014/06/19 00:19:10 by garm             ###   ########.fr       ##
#                                                                              #
# **************************************************************************** #

CC = gcc

ifeq ($(HOSTTYPE),)
	HOSTTYPE := $(shell uname -m)_$(shell uname -s)
endif

NAME = libft_malloc_$(HOSTTYPE).so
NAME_LINK = libft_malloc.so

SOURCES_DIR = srcs
INCLUDES_DIR = includes

CFLAGS =  $(FLAGS) -I $(INCLUDES_DIR)

DEPENDENCIES = \
			   $(INCLUDES_DIR)/malloc.h

SOURCES = \
		  $(SOURCES_DIR)/show_alloc_mem.c \
		  $(SOURCES_DIR)/ft_malloc.c \
		  $(SOURCES_DIR)/ft_chunk.c \
		  $(SOURCES_DIR)/ft_split.c \
		  $(SOURCES_DIR)/ft_free.c \
		  $(SOURCES_DIR)/ft_defragment.c \
		  $(SOURCES_DIR)/ft_realloc.c \
		  $(SOURCES_DIR)/ft_putnbr_base.c \
		  $(SOURCES_DIR)/ft_utils.c \
		  \

OBJS = $(SOURCES:.c=.o)

all: $(NAME)

$(NAME):
	@echo Creating $(NAME)...
	$(CC) -shared -o $(NAME) $(SOURCES) -I $(INCLUDES_DIR)
	@echo Creating $(NAME_LINK)...
	@ln -s $(NAME) $(NAME_LINK)

clean:
	@rm -f $(NAME_LINK)
	@echo Deleting Symbolic link : $(NAME_LINK)...

fclean: clean
	@rm -f $(NAME)
	@echo Deleting $(NAME)...

re: fclean all

.PHONY: clean fclean re all test

